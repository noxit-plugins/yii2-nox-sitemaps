<?php

    namespace nox\modules\sitemaps;

    use nox\modules\sitemaps\behaviors\SitemapBehavior;
    use yii\base\InvalidConfigException;
    use yii\base\Model;
    use yii\caching\Cache;

    /**
     * Class Module
     *
     * @package nox\modules\sitemaps
     */
    class Module extends \yii\base\Module
    {
        /**
         * @var string
         */
        public $controllerNamespace = 'nox\modules\sitemaps\controllers';

        /**
         * @var int
         */
        public $cacheExpire = 86400;

        /**
         * @var string|Cache
         */
        public $cacheProvider = 'cache';

        /**
         * @var string
         */
        public $cacheKey = 'sitemap';

        /**
         * @var bool
         */
        public $enableGzip = false;

        /**
         * @var array
         */
        public $models = [];

        /**
         * @var array
         */
        public $urls = [];

        /**
         * @inheritdoc
         */
        public function init()
        {
            parent::init();

            if (is_string($this->cacheProvider)) {
                $this->cacheProvider = \Yii::$app->{$this->cacheProvider};
            }

            if (!$this->cacheProvider instanceof Cache) {
                throw new InvalidConfigException('Invalid `cacheKey` parameter was specified.');
            }
        }

        /**
         * Build and cache a site map.
         *
         * @return string
         * @throws InvalidConfigException
         */
        public function buildSitemap()
        {
            $urls = $this->urls;

            foreach ($this->models as $modelName) {
                /** @var SitemapBehavior|Model $model */
                if (is_array($modelName)) {
                    $model = new $modelName['class'];
                    if (isset($modelName['behaviors'])) {
                        $model->attachBehaviors($modelName['behaviors']);
                    }
                } else {
                    $model = new $modelName;
                }

                $urls = array_merge($urls, $model->generateSiteMap());
            }

            $sitemapData = $this->createControllerByID('default')->renderPartial('index', ['urls' => $urls]);

            $this->cacheProvider->set($this->cacheKey, $sitemapData, $this->cacheExpire);

            return $sitemapData;
        }
    }
