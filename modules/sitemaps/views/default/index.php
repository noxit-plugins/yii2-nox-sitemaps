<?php

    /**
     * @var array $urls
     */

    use nox\helpers\UrlHelper;

    echo '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9">
    <?php foreach ($urls as $url) : ?>
    <url>
        <loc><?= UrlHelper::to($url['loc'], true); ?></loc>

        <?php if (isset($url['lastmod'])): ?><lastmod><?= ((is_string($url['lastmod'])) ? $url['lastmod'] : date(DATE_W3C, $url['lastmod'])); ?></lastmod><?php endif; ?>

        <?php if (isset($url['changefreq'])): ?><changefreq><?= $url['changefreq']; ?></changefreq><?php endif; ?>

        <?php if (isset($url['priority'])): ?><priority><?= $url['priority']; ?></priority><?php endif; ?>

        <?php if (isset($url['news'])): ?>
        <news:news>
            <news:publication>
                <news:name><?= $url['news']['publication']['name']; ?></news:name>
                <news:language><?= $url['news']['publication']['language']; ?></news:language>
            </news:publication>
            <?php if (isset($url['news']['access'])) : ?><news:access><?= $url['news']['access']; ?></news:access><?php endif; ?>
            <?php if (isset($url['news']['genres'])) : ?><news:genres><?= $url['news']['genres']; ?></news:genres><?php endif; ?>
            <news:publication_date><?= ((is_string($url['news']['publication_date'])) ? $url['news']['publication_date'] : date(DATE_W3C, $url['news']['publication_date'])); ?></news:publication_date>
            <news:title><?= $url['news']['title']; ?></news:title>
            <?php if (isset($url['news']['keywords'])) : ?><news:keywords><?= $url['news']['keywords']; ?></news:keywords><?php endif; ?>
            <?php if (isset($url['news']['stock_tickers'])) : ?><news:stock_tickers><?= $url['news']['stock_tickers']; ?></news:stock_tickers><?php endif; ?>
        </news:news>
        <?php endif; ?>

        <?php if (isset($url['images'])) : ?>
        <?php foreach ($url['images'] as $image): ?>
        <image:image>
            <image:loc><?= UrlHelper::to($image['loc'], true) ?></image:loc>

            <?php if (isset($image['caption'])) : ?><image:caption><?= $image['caption']; ?></image:caption><?php endif; ?>
            <?php if (isset($image['geo_location'])) : ?><image:geo_location><?= $image['geo_location']; ?></image:geo_location><?php endif; ?>
            <?php if (isset($image['title'])) : ?><image:title><?= $image['title']; ?></image:title><?php endif; ?>
            <?php if (isset($image['license'])) : ?><image:license><?= $image['license']; ?></image:license><?php endif; ?>
        </image:image>
        <?php endforeach; ?>
        <?php endif; ?>
    </url>
    <?php endforeach; ?>
</urlset>
