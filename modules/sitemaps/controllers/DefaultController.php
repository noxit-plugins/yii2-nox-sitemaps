<?php

    namespace nox\modules\sitemaps\controllers;

    use nox\modules\sitemaps\Module;
    use Yii;
    use yii\web\Controller;
    use yii\web\Response;

    /**
     * Class DefaultController
     *
     * @package nox\modules\sitemaps\controllers
     */
    class DefaultController extends Controller
    {
        /**
         * @return string
         */
        public function actionIndex()
        {
            /** @var Module $module */
            $module = $this->module;

            if (!$sitemapData = $module->cacheProvider->get($module->cacheKey)) {
                $sitemapData = $module->buildSitemap();
            }

            Yii::$app->response->format = Response::FORMAT_RAW;

            $headers = Yii::$app->response->headers;
            
            $headers->add('Content-Type', 'application/xml');
            
            if ($module->enableGzip) {
                $sitemapData = gzencode($sitemapData);
                $headers->add('Content-Encoding', 'gzip');
                $headers->add('Content-Length', strlen($sitemapData));
            }

            return $sitemapData;
        }
    }
