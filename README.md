Yii PHP Framework Version 2 / NOX XML Sitemap Generator
=======================================================

Yii2 Module for automatically generating [XML Sitemap](http://www.sitemaps.org/protocol.html).

Installation
------------
The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

* Either run

```
php composer.phar require --prefer-dist "nox-it/yii2-nox-sitemaps" "*"
```

or add

```json
"nox-it/yii2-nox-sitemaps" : "*"
```

to the `require` section of your application's `composer.json` file.

* Configure the `cache` component of your application's configuration file, for example:

```php
'components' => [
    'cache' => [
        'class' => 'yii\caching\FileCache',
    ],
]
```

* Add a new module in `modules` section of your application's configuration file, for example:

```php
'modules' => [
    'sitemaps' => [
        'class' => 'nox\modules\sitemaps\Module',
        'models' => [
            // your models
            'app\modules\news\models\News',
            // or configuration for creating a behavior
            [
                'class' => 'app\modules\news\models\News',
                'behaviors' => [
                    'sitemap' => [
                        'class' => SitemapBehavior::className(),
                        'scope' => function ($model) {
                            /** @var \yii\db\ActiveQuery $model */
                            $model->select(['url', 'lastmod']);
                            $model->andWhere(['is_deleted' => 0]);
                        },
                        'dataClosure' => function ($model) {
                            /** @var self $model */
                            return [
                                'loc' => Url::to($model->url, true),
                                'lastmod' => strtotime($model->lastmod),
                                'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                                'priority' => 0.8
                            ];
                        }
                    ]
                ]
            ]
        ],
        'urls'=> [
            // your additional urls
            [
                'loc' => '/news/index',
                'changefreq' => \nox\modules\sitemaps\behaviors\SitemapBehavior::CHANGEFREQ_DAILY,
                'priority' => 0.8,
                'news' => [
                    'publication'   => [
                        'name'          => 'Example Blog',
                        'language'      => 'en',
                    ],
                    'access'            => 'Subscription',
                    'genres'            => 'Blog, UserGenerated',
                    'publication_date'  => 'YYYY-MM-DDThh:mm:ssTZD',
                    'title'             => 'Example Title',
                    'keywords'          => 'example, keywords, comma-separated',
                    'stock_tickers'     => 'NASDAQ:A, NASDAQ:B',
                ],
                'images' => [
                    [
                        'loc'           => 'http://example.com/image.jpg',
                        'caption'       => 'This is an example of a caption of an image',
                        'geo_location'  => 'City, State',
                        'title'         => 'Example image',
                        'license'       => 'http://example.com/license',
                    ]
                ]
            ]
        ],
        'enableGzip' => true, // default is false
        'cacheExpire' => 1, // 1 second. Default is 24 hours
    ],
],
```

* Add behavior in the AR models, for example:

```php
use nox\modules\sitemaps\behaviors\SitemapBehavior;

public function behaviors()
{
    return [
        'sitemap' => [
            'class' => SitemapBehavior::className(),
            'scope' => function ($model) {
                /** @var \yii\db\ActiveQuery $model */
                $model->select(['url', 'lastmod']);
                $model->andWhere(['isDeleted' => 0]);
            },
            'dataClosure' => function ($model) {
                /** @var static $model */
                return [
                    'loc'        => Url::to($model->url, true),
                    'lastmod'    => strtotime($model->lastmod),
                    'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                    'priority'   => 0.8
                ];
            }
        ]
    ];
}
```

* Add a new rule for `urlManager` of your application's configuration file, for example:

```php
'urlManager' => [
    'rules' => [
        ['pattern' => 'sitemap', 'route' => 'sitemaps/default/index', 'suffix' => '.xml'],
    ],
],
```

Resources
---------
* [XML Sitemap](http://www.sitemaps.org/protocol.html)

* [News Sitemap](https://support.google.com/news/publisher/answer/74288?hl=en)

* [Image Sitemaps](https://support.google.com/webmasters/answer/178636?hl=en)

Other Informations
------------------
This module was based on [himiklab/yii2-sitemap-module](https://github.com/himiklab/yii2-sitemap-module).

![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)
